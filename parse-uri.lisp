;==============================================================================
;                            MEMBRI GRUPPO PROGETTO
;==============================================================================
;
;    NOME/COG: Dario Mantegazza
;    NOME/COG: Luca Samuele Vanzo
;    NOME/COG: Francesco Wild
;
;==============================================================================
;                       INFORMAZIONI/RICHIESTE PROGETTO
;==============================================================================
;
;    Linguaggi di Programmazione
;    Modulo di Laboratorio di Linguaggi di Programmazione 
;    Progetto Lisp Febbraio 2012
;
;    Parsing di stringhe URI
;    Marco Antoniotti e Giuseppe Vizzari
; 
;    - INTRODUZIONE -
;    La navigazione web, ma non solo, richiede ad un programma ed ai suoi
;    programmatori l’abilità di manipolare delle stringhe che rappresentano 
;    degli “Universal Resource Identifiers” (URI). Lo scopo di questo progetto 
;    è di realizzare una libreria in Common Lisp che costruisca delle 
;    strutture che rappresentino internamente delle URI a partire dalla loro 
;    rappresentazione come stringhe.
;
;    - GITHUB -
;    La versione del Parser proposta può contenere:
;
;          -> Errori.
;          -> Valutare l'URI non correttamente.
;          -> Accettare/Negare casi validi e non.
;
;    In breve: 
;    Non è da considerare una versione definitiva e priva di bug.
;    Buon uso e lettura. Enjoy! :-)
;
;==============================================================================
;                           DEFINIZIONE VARIABILI
;==============================================================================

(defvar uri-structure '())                                    ; STRUTTURA URI
(defvar uri-type-parser 0)                                    ; TIPO PARSER
(defvar ide-to-find (list '#\/ '#\? '#\# '#\@ '#\:))          ; IDENTIFICATORI

;==============================================================================
;                              MAPPATURA ERRORI
;==============================================================================

(defun spawn-error (err-id)
    (cond
        ;----------------------------------------------------------------------
        ((= err-id 0)                                       ; ERRORE ID:      0
            (write-line "Ops! Si e' verificato un errore non previsto! 
                        Un gruppo di scimmie volanti sistemera' quanto prima!")
        )
        ;----------------------------------------------------------------------
        ((= err-id 1)                                       ; ERRORE ID:      1
            (write-line "ATTENZIONE: E' necessario eseguire prima 
                         (parse-uri url)!")
        )
        ;----------------------------------------------------------------------
        ((= err-id 2)                                       ; ERRORE ID:      2
            (write-line "ATTENZIONE: PARAMETRO URI NON INSERITO!")
        )
        ;----------------------------------------------------------------------
        ((= err-id 3)                                       ; ERRORE ID:      3
            (write-line "ATTENZIONE: L'URI INSERITA NON E' UNA STRINGA!")
        )
        ;----------------------------------------------------------------------
        (T nil)
        ;----------------------------------------------------------------------
    )

    (clear-output)
)

;==============================================================================
;                        FUNZIONE DI SALVATAGGIO URI
;==============================================================================

(defun save-type (type-to-set)
    ;--------------------------------------------------------------------------
    (setq                                                ; Assegno:
        uri-type-parser                                  ;  -> Variabile
        type-to-set                                      ;  -> Valore
    )                                                    ; End SETQ
    ;--------------------------------------------------------------------------
)

(defun save-data (data-to-save)
    (cond
        ;----------------------------------------------------------------------
        ((not (null data-to-save))                      ; Se non NULL
            (setq uri-structure                         ; Salvataggio parametro
                (append uri-structure                   ; Aggiungo al Buffer
                    (list                               ; Deve essere una lista
                        (coerce                         ; Converto in stringa
                            data-to-save                ; (list)
                            'string                     ; (string)
                        )                               ; End conversione
                    )                                   ; End LIST
                )                                       ; End APPEND
            )                                           ; End Salvataggio
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        ((null data-to-save)                            ; Se NULL
            (setq uri-structure                         ; Salvataggio parametro
                (append uri-structure                   ; Aggiungo al Buffer
                    (list nil)                          ; Aggiungo Elemento NIL
                )                                       ; End APPEND
            )                                           ; End SETQ
        )                                               ; End COND-2
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                       CONTROLLI DIMENSIONE SU LISTA
;==============================================================================

(defun is-empty-list (c)
    (cond
        ;----------------------------------------------------------------------
        ((null c) T)                                    ; Se vuota => TRUE
        ;----------------------------------------------------------------------
        (T nil)                                         ; TRUE => NULL
        ;----------------------------------------------------------------------
    )
)

(defun is-one-element (c)
    (cond
        ;----------------------------------------------------------------------
        ((and                                           ; Stant check: AND
                (car c)                                 ; 1° char != NULL
                (null (cdr c))                          ; 2° == NULL
            )                                           ; End check: AND
            T                                           ; TRUE
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        (T nil)                                         ; TRUE => NULL
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                          CONTROLLO IDENTIFICATORI
;==============================================================================

(defun is-host-ok (uri-source)
    (cond
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (is-one-element uri-source)             ; Controllo contenuto
                (equal (car uri-source) '#\.)           ; È == carattere "." ?
            )                                           ; End check: AND
            (spawn-error 0)                             ; Error
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (equal (car uri-source) '#\.)           ; Due o più punti ->
                (equal (cadr uri-source) '#\.)          ; -> consecutivi
            )                                           ; End check: AND
            (spawn-error 0)                             ; Error
        )                                               ; End COND-2
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (equal (car uri-source) '#\.)           ; Termina con un "."?
                (not                                    ;          |
                    (is-ide-ok                          ;          |
                        (cadr uri-source)               ;          |
                        ide-to-find                     ;          |
                    )                                   ;          |
                )                                       ;          |
            )                                           ; End check: AND
            (spawn-error 0)                             ; Error
        )                                               ; End COND-3
        ;----------------------------------------------------------------------
        ((or                                            ; Start check: OR
                (is-ide-ok (car uri-source) ide-to-find); Check identificatori
                (equal (cadr uri-source) '#\.)          ; 2° ch == ch-to-find
            )                                           ; End check: OR
            T                                           ; TRUE
        )                                               ; End COND-4
        ;----------------------------------------------------------------------
        (T (spawn-error 0))                             ; Error
        ;----------------------------------------------------------------------
    )
)

(defun is-ide-ok (x ide-to-find)
    (cond
        ;----------------------------------------------------------------------
        ((equal x (car ide-to-find))                    ; 1° ch-Y == X
            nil                                         ; Return NULL
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        ((or                                            ; Start check: OR
                (is-one-element ide-to-find)            ; Controllo contenuto
                (null x)                                ; NULL
            )                                           ; End check: OR 
            T                                           ; TRUE
        )                                               ; End COND-2
        ;----------------------------------------------------------------------
        ((not (equal (car ide-to-find) x))              ; 1° ch-Y != X
            (is-ide-ok                                  ; Ricorsione
                x                                       ; Carattere
                (cdr ide-to-find)                       ; Lista senza 1° elem.
            )                                           ; End IS-IDE-OK
        )                                               ; End COND-3
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                         CONTROLLO NUMERICO CHAR
;==============================================================================

(defun char-is-number (char-to-check)
    (cond 
        ;----------------------------------------------------------------------
        ((not (equal (digit-char-p char-to-check) nil)) ; È diverso da NIL?
            T                                           ; È un numero
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        (T nil)                                         ; Non è un numero
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                            CONTROLLO AUTHORITHY
;==============================================================================

(defun control-authority (uri-source)
    (cond
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (is-one-element uri-source)             ; Controllo contenuto
                (not (equal (car uri-source) '#\@))     ; 1° ch != ch-to-find
            )                                           ; End check: AND
            (save-data nil)                             ; USER-INFO
            nil                                         ; Return FALSE
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (is-one-element uri-source)             ; Controllo contenuto 
                (equal (car uri-source) '#\@)           ; 1° ch == ch-to-find
            )                                           ; End check: AND
            (spawn-error 0)                             ; Error
        )                                               ; End COND-2
        ;----------------------------------------------------------------------
        ((equal (car uri-source) '#\@)                  ; 1° ch == ch-to-find
            T                                           ; TRUE
        )                                               ; End COND-3
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (not (equal (car uri-source) '#\:))     ; 1° ch != ":"
                (not (is-ide-ok                         ; Controllo validità
                    (car uri-source)                    ;          |
                    ide-to-find)                        ;          |
                )                                       ;          |
            )                                           ; End check: AND
            (save-data nil)                             ; USER-INFO
            nil                                         ; Return FALSE
        )                                               ; End COND-4
        ;----------------------------------------------------------------------
        ((not (equal (car uri-source) '#\@))            ; 1° ch != ch-to-find
            (control-authority (cdr uri-source))        ; È presente l'User?
        )                                               ; End COND-5
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                              CONTROLLO USER
;==============================================================================

(defun control-user (uri-source temp)
    (cond
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (is-ide-ok (car uri-source) ide-to-find); Controllo identific.
                (is-one-element uri-source)             ; Controllo contenuto
            )                                           ; End check: AND
            (spawn-error 0)                             ; Error
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        ((or                                            ; Start check: OR 
                (not (equal (car uri-source) '#\@))     ; 1° ch != ch-to-find
                (is-ide-ok                              ; Controllo validità 
                    (car uri-source)                    ;            |
                    ide-to-find                         ;            |
                )                                       ;            |
            )                                           ; End check: OR
            (control-user                               ; Ricorsione...
                (cdr uri-source)                        ; URI senza 1° char
                (append                                 ; Buffer...
                    temp                                ;           |
                    (coerce                             ;           |
                        (string (car uri-source))       ;           |
                        'list                           ;           |
                    )                                   ;           |
                )                                       ;           |
            )                                           ; End ricorsione
        )                                               ; End COND-2
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (equal (car uri-source) '#\@)           ; 1° ch == ch-to-find
                (and                                    ; Start check: AND
                        (not                            ; Controllo contenuto
                            (is-one-element uri-source) ;           |
                        )                               ;           |    
                        (not (equal temp nil))          ; Buffer è pieno?
                    )                                   ; End check: AND
            )                                           ; End check: AND
            (save-data temp)                            ; Salvo Struttura
            (if (= uri-type-parser 1)                   ; È del tipo 1?
                (control-host (cdr uri-source) nil)     ; Tipo 1
                (control-host-tre (cdr uri-source) nil) ; Tipo 3
            )                                           ; End check: IF
        )                                               ; End COND-3
        ;----------------------------------------------------------------------
        (T (spawn-error 0))                             ; Error
        ;----------------------------------------------------------------------
    )
)

(defun control-user-tre (uri-source temp)
    (cond
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (is-one-element  uri-source)            ; Controllo contenuto
                (is-ide-ok (car uri-source) ide-to-find); Controllo validità
            )                                           ; End check: AND
            (save-data                                  ; Salvo Struttura
                (append                                 ;          |
                    temp                                ;          |
                    (list (car uri-source))             ;          |
                )                                       ;          |
            )                                           ; End Salvataggio
            (save-data nil)                             ; HOST
            (save-data nil)                             ; PORTA
            (save-data nil)                             ; PATH
            (save-data nil)                             ; QUERY
            (save-data nil)                             ; FRAGMENT
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        ((equal (car uri-source) '#\@)                  ; 1° ch == ch-to-find
            (spawn-error 0)                             ; Error
        )                                               ; End COND-2
        ;----------------------------------------------------------------------
        ((not (is-ide-ok (car uri-source) ide-to-find)) ; 1° ch != ch-to-find
            (spawn-error 0)                             ; Error
        )                                               ; End COND-3
        ;----------------------------------------------------------------------
        ((is-ide-ok (car uri-source) ide-to-find)       ; Validità identific.
            (control-user-tre                           ; Call Function
                (cdr uri-source)                        ; URI senza 1° char
                (append temp (list (car uri-source)))   ; Buffer
            )                                           ; End FUNCTION
        )                                               ; End COND-4
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                                CONTROLLO PORT
;==============================================================================

(defun control-port(uri-source temp)
    (cond
        ;----------------------------------------------------------------------
        ((equal (car uri-source) '#\/)                  ; 1° ch == ch-to-find
            (save-data temp)                            ; Salvataggio
            (control-path uri-source nil)               ; Call Function
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        ((equal (car uri-source) '#\?)                  ; 1° ch == ch-to-find
            (save-data temp)                            ; Salvo Struttura
            (save-data nil)                             ; PATH
            (control-query (cdr uri-source) nil)        ; Call Function
        )                                               ; End COND-2
        ;----------------------------------------------------------------------
        ((equal (car uri-source) '#\#)                  ; 1° ch == ch-to-find
            (save-data temp)                            ; Salvo Struttura
            (save-data nil)                             ; PATH
            (save-data nil)                             ; QUERY
            (control-fragment (cdr uri-source) nil)     ; Call Function
        )                                               ; End COND-3
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (is-one-element uri-source)             ; Controllo contenuto
                (char-is-number (car uri-source))       ; È un numero?
            )                                           ; End check: AND
            (save-data temp)                            ; Salvo Struttura
            (save-data nil)                             ; PATH
            (save-data nil)                             ; QUERY
            (save-data nil)                             ; FRAGMENT
        )                                               ; End COND-4
        ;----------------------------------------------------------------------
        ((char-is-number (car uri-source))              ; È un numero?
            (control-port                               ; Call Function
                (cdr uri-source)                        ;          |
                (append temp (list(car uri-source)))    ;          |
            )                                           ; End FUNCTION
        )                                               ; End COND-5
        ;----------------------------------------------------------------------
        (T (spawn-error 0))                             ; Error
        ;----------------------------------------------------------------------
    )
)    

;==============================================================================
;                                CONTROLLO PATH
;==============================================================================

(defun control-path (uri-source temp)
    (cond
        ;----------------------------------------------------------------------
        ((is-empty-list uri-source)                     ; È una lista vuota?
            (if (equal temp nil)                        ; È il buffer vuoto?
                (save-data nil)                         ; PATH
                (save-data temp)                        ; Salvo Struttura
            )                                           ; End check: IF
            (save-data nil)                             ; QUERY
            (save-data nil)                             ; FRAGMENT
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (is-one-element uri-source)             ; Controllo contenuto
                (equal (car uri-source) '#\/)           ; Controllo validità
            )                                           ; End check: AND
            (save-data                                  ; Salvo Struttura
                (append                                 ; Buffer...
                    temp                                ;          |
                    (list (car uri-source))             ;          |
                )                                       ;          |
            )                                           ;          |
            (save-data nil)                             ; QUERY
            (save-data nil)                             ; FRAGMENT
        )                                               ; End COND-2
        ;----------------------------------------------------------------------
        ((equal (car uri-source) '#\?)                  ; 1° ch == ch-to-find
            (save-data temp)                            ; Salvo Struttura 
            (control-query (cdr uri-source) nil)        ; Call Function
        )                                               ; End COND-3
        ;----------------------------------------------------------------------
        ((equal (car uri-source) '#\#)                  ; 1° ch == ch-to-find
            (save-data temp)                            ; Salva Struttura
            (save-data nil)                             ; QUERY
            (control-fragment (cdr uri-source) nil)     ; Call Function
        )                                               ; End COND-4
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (equal (car uri-source) '#\/)           ; Due o più / ->
                (equal (cadr uri-source) '#\/)          ; -> consecutivi
            )                                           ; End check: AND
            (spawn-error 0)                             ; Error
        )                                               ; End COND-5
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (not (equal (car uri-source) '#\/))     ; 1° ch != ch-to-find
                (not                                    ; Controllo validità
                    (is-ide-ok                          ;           |
                        (car uri-source)                ;           |
                        ide-to-find                     ;           |
                    )                                   ;           |
                )                                       ;           |
            )                                           ; End check: AND
            (save-data nil)                             ; PATH
            (save-data nil)                             ; QUERY
            (save-data nil)                             ; FRAGMENT
            (spawn-error 0)                             ; Error
        )                                               ; End COND-6
        ;----------------------------------------------------------------------
        (T                                              ; TRUE
            (control-path                               ; Ricorsione...
                (cdr uri-source)                        ; URI senza 1° char
                (append                                 ; Buffer...
                    temp                                ;          |
                    (list (car uri-source))             ;          |
                )                                       ;          |
            )                                           ; End Ricorsione
        )                                               ; End COND-7
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                                CONTROLLO QUERY
;==============================================================================

(defun control-query (uri-source temp)
    (cond
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (equal temp nil)                        ; È temp nullo?
                (is-empty-list uri-source)              ; È URI vuota?
            )                                           ; End check: AND
            (save-data nil)                             ; QUERY
            (save-data nil)                             ; FRAGMENT
            (spawn-error 0)                             ; Error
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (equal temp nil)                        ; È temp nullo?
                (equal (car uri-source) '#\#)           ; 1° ch == ch-to-find
            )                                           ; End check: AND
            (save-data nil)                             ; QUERY
            (save-data nil)                             ; FRAGMENT
            (spawn-error 0)                             ; Error
        )                                               ; End COND-2
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (is-one-element uri-source)             ; Controllo contenuto
                (not (equal (car uri-source) '#\#))     ; 1° ch != ch-to-find
            )                                           ; End check: AND
            (save-data                                  ; Salvo Struttura
                (append                                 ;           |
                    temp                                ;           |
                    (list (car uri-source))             ;           |
                )                                       ;           |
            )                                           ; End Salvataggio
            (save-data nil)                             ; FRAGMENT
        )                                               ; End COND-3
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (is-one-element uri-source)             ; Controllo contenuto
                (equal (car uri-source) '#\#)           ; 1° ch == ch-to-find
            )                                           ; End check: AND
            (save-data temp)                            ; Salvo Struttura
            (save-data nil)                             ; FRAGMENT
        )                                               ; End COND-4
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (not (is-one-element uri-source))       ; Controllo contenuto
                (equal (car uri-source) '#\#)           ; 1° ch == ch-to-find
            )                                           ; End check: AND
            (save-data temp)                            ; Salvo Struttura
            (control-fragment (cdr uri-source) nil)     ; Call Fuction
        )                                               ; End COND-5
        ;----------------------------------------------------------------------
        (T                                              ; TRUE
            (control-query                              ; Call Function
                (cdr uri-source)                        ; URI senza 1° char
                (append temp (list (car uri-source)))   ; Buffer
            )                                           ; End FUNCTION
        )                                               ; End COND-6
        ;----------------------------------------------------------------------
    )
)    

;==============================================================================
;                              CONTROLLO FRAGMENT
;==============================================================================

(defun control-fragment (uri-source temp)
    (cond
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (equal temp nil)                        ; È temp nullo?
                (is-empty-list uri-source)              ; È URI vuoto?
            )                                           ; End check: AND
            (save-data nil)                             ; FRAGMENT
            (spawn-error 0)                             ; Error
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        ((is-one-element uri-source)                    ; Controllo contenuto
            (save-data                                  ; Salvo Struttura
                (append                                 ;         |
                    temp                                ;         |
                    (list (car uri-source))             ;         |
                )                                       ;         |
            )                                           ; End Salvataggio
        )                                               ; End COND-2
        ;----------------------------------------------------------------------
        ((not (is-empty-list uri-source))               ; Se non è vuota
            (control-fragment                           ; Call Function
                (cdr uri-source)                        ; URi senza 1° char
                (append temp (list (car uri-source)))   ; Buffer
            )                                           ; End FUNCTION
        )                                               ; End COND-3
        ;----------------------------------------------------------------------
        (T (save-data nil))                             ; Error
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                             CONTROLLO IP/HOST
;==============================================================================

(defun control-host (uri-source temp)
    (cond
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (equal temp nil)                        ; È temp nullo?
                (equal (car uri-source) '#\.)           ; 1° ch == ch-to-find
            )                                           ; End check: AND
            (spawn-error 0)                             ; Error
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (is-one-element uri-source)             ; Controllo contenuto
                (is-host-ok uri-source)                 ; Controllo validità
            )                                           ; End check: AND
            (save-data                                  ; Salvo Struttura
                (append                                 ;         |
                    temp                                ;         |
                    (list(car uri-source))              ;         |
                )                                       ;         |
            )                                           ; End Salvataggio
            (save-data nil)                             ; PORTA
            (save-data nil)                             ; PATH
             (save-data nil)                            ; QUERY
            (save-data nil)                             ; FRAGMENT
        )                                               ; End COND-2
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (not (equal temp nil))                  ; È temp nullo?
                (equal (car uri-source) '#\/)           ; 1° ch == ch-to-find
            )                                           ; End check: AND
            (save-data temp)                            ; Salvo Struttura
            (save-data nil)                             ; PORTA
            (control-path                               ; Ricorsione... 
                uri-source                              ;  -> Passo URI - 1° ch
                nil                                     ;  -> NULL
            )                                           ; End FUNCTION
        )                                               ; End COND-3
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (not (equal temp nil))                  ; È temp nullo?
                (equal (car uri-source) '#\?)           ; 1° ch == ch-to-find
            )                                           ; End check: AND
            (save-data temp)                            ; Salvo Struttura
            (save-data nil)                             ; PORTA
            (save-data nil)                             ; PATH
            (control-query                              ; Ricorsione... 
                (cdr uri-source)                        ;  -> Passo URI - 1° ch
                nil                                     ;  -> NULL
            )                                           ; End FUNCTION
        )                                               ; End COND-4
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (not (equal temp nil))                  ; È temp nullo?
                (equal (car uri-source) '#\#)           ; 1° ch == ch-to-find
            )                                           ; End check: AND
            (save-data temp)                            ; Salvo Struttura
            (save-data nil)                             ; PORTA
            (save-data nil)                             ; PATH
            (save-data nil)                             ; QUERY
            (control-fragment                           ; Ricorsione...
                (cdr uri-source)                        ;  -> Passo URI - 1° ch
                nil                                     ;  -> NULL
            )                                           ; End FUNCTION
        )                                               ; End COND-5
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (not (equal temp nil))                  ; È temp nullo?
                (equal (car uri-source) '#\:)           ; 1° ch == ch-to-find
            )                                           ; End check: AND
            (save-data temp)                            ; Salvo Struttura
            (control-port                               ; Ricorsione... 
                (cdr uri-source)                        ;  -> Passo URI - 1° ch
                nil                                     ; NULL
            )                                           ; End Function
        )                                               ; End COND-6
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (not (is-one-element uri-source))       ; Controllo contenuto
                (is-host-ok uri-source)                 ; Validità HOST
            )                                           ; End check: AND
            (control-host                               ; Ricorsione...
                (cdr uri-source)                        ; -> Passo URI - 1° ch
                (append temp (list (car uri-source)))   ; -> Buffer
            )                                           ; End FUNCTION
        )                                               ; End COND-7
        ;----------------------------------------------------------------------
        (T (spawn-error 0))                             ; Error
        ;----------------------------------------------------------------------
    )
)

(defun control-host-tre (uri-source temp)
    (cond
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (equal temp nil)                        ; È temp vuoto?
                (is-empty-list uri-source)              ; È URI vuota?
            )                                           ; End check: AND
            (save-data nil)                             ; HOST
            (save-data nil)                             ; PORTA
            (save-data nil)                             ; PATH
            (save-data nil)                             ; QUERY
            (save-data nil)                             ; FRAGMENT
            (spawn-error 0)                             ; Error
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (is-one-element uri-source)             ; Controllo contenuto
                (is-host-ok uri-source)                 ; Controllo validità
            )                                           ; End check: AND
            (save-data                                  ; Salvo Struttura
                (append                                 ;         |
                    temp                                ;         |
                    (list(car uri-source))              ;         |
                )                                       ;         |
            )                                           ; End Salvataggio
            (save-data nil)                             ; PORTA
            (save-data nil)                             ; PATH
            (save-data nil)                             ; QUERY
            (save-data nil)                             ; FRAGMENT
        )                                               ; End COND-2
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (not (is-one-element uri-source))       ; Controllo contenuto
                (is-host-ok uri-source)                 ; Validità HOST
            )                                           ; End check: AND
            (control-host-tre                           ; Call Function
                (cdr uri-source)                        ; URI senza 1° char
                (append temp (list (car uri-source)))   ; Buffer
            )                                           ; End FUNCTION
        )                                               ; End COND-3
        ;----------------------------------------------------------------------
        (T (spawn-error 0))                             ; Error
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                          CONTROLLO SCHEMA URI
;==============================================================================

(defun control-scheme (uri-source schema-tmp)
      (cond 
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                (not (equal (car uri-source) '#\:))     ; 1° ch != ch-to-find
                (is-one-element uri-source)             ; Controllo contenuto
            )                                           ; End check: AND
            (spawn-error 0)                             ; Error
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
           ((and                                        ; Start check: AND
                   (not (equal (car uri-source) '#\:))  ; 1° ch != :
                   (not (equal (car uri-source) '#\/))  ; 1° ch != /
               )                                        ; End check: AND
               (control-scheme                          ; Call function
                   (cdr uri-source)                     ; Uri senza 1° char
                   (append schema-tmp (                 ; Buffer
                       list (car uri-source))           ; URI
                   )                                    ; End Buffer
               )                                        ; End function
           )                                            ; End COND-2
        ;----------------------------------------------------------------------
        ((equal (car uri-source) '#\:)                  ; 1° ch == ":"
            (save-data schema-tmp)                      ; Salva Struttura
            (control-scheme-helper                      ; Call Function
                (cdr uri-source)                        ; Passo l'URI
            )                                           ; End FUNC
        )                                               ; End COND-3
        ;----------------------------------------------------------------------
        (T (spawn-error 0))                             ; Error
        ;----------------------------------------------------------------------
    )
)

(defun control-scheme-helper (uri-source)
    (cond 
        ;----------------------------------------------------------------------
        ((is-empty-list uri-source)                     ; È URI vuoto?
            (save-data nil)                             ; USERINFO
            (save-data nil)                             ; HOST
            (save-data nil)                             ; PORT
            (save-data nil)                             ; PATH
            (save-data nil)                             ; QUERY
            (save-data nil)                             ; FRAGMENT
        )                                               ; End COND-1
        ;----------------------------------------------------------------------
        ((equalp (car uri-structure) "mailto")          ; 1° elem == "mailto"
            (save-type 3)                               ; Parser Type: 3
            (control-user uri-source nil)               ; Call Function 
        )                                               ; End COND-2
        ;----------------------------------------------------------------------
        ((equalp (car uri-structure) "news")            ; 1° elem == "news"
            (save-data nil)                             ; USERINFO
            (control-host-tre uri-source nil)           ; Call Function
        )                                               ; End COND-3
        ;----------------------------------------------------------------------
        ((or                                            ; Start check: OR
                (equalp (car uri-structure) "tel")      ; 1° elem == "tel"
                (equalp (car uri-structure) "fax")      ; 1° elem == "fax"
            )                                           ; End check: OR
            (control-user-tre uri-source nil)           ; Call Function
        )                                               ; End COND-4
        ;----------------------------------------------------------------------
        ((and                                           ; Start check: AND
                   (equal (car uri-source) '#\/)        ; 1° ch == ch-to-find
                   (equal (car (cdr uri-source)) '#\/)  ; 1° ch == ch-to-find
            )                                           ; End check: AND
            (save-type 1)                               ; Parser Type: 1
            (if                                         ; Start check: IF
                (control-authority (cddr uri-source))   ; È presente l'User?
                (control-user (cddr uri-source) nil)    ; Call FUNCTION
                (control-host (cddr uri-source) nil)    ; Call FUNCTION
            )                                           ; End check: IF
        )                                               ; End COND-5
        ;----------------------------------------------------------------------
        (T                                              ; TRUE
            (save-data nil)                             ; USERINFO
            (save-data nil)                             ; HOST
            (save-data nil)                             ; PORT
            (control-path uri-source nil)               ; TRUE
        )                                               ; End COND-6 
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                            FUNZIONI DI SUPPORTO
;==============================================================================

(defun uri-scheme (uri-structure)
    (if 
        (not (is-empty-list uri-structure))
        (first uri-structure)
        (spawn-error 1)
    )
)

(defun uri-userinfo (uri-structure)
    (if 
        (not (is-empty-list uri-structure))
        (second uri-structure)    
        (spawn-error 1)
    )
)

(defun uri-host (uri-structure)
    (if 
        (not (is-empty-list uri-structure))
        (third uri-structure)
        (spawn-error 1)
    )
)

(defun uri-port (uri-structure)
    (if 
        (not (is-empty-list uri-structure))
        (fourth uri-structure)
        (spawn-error 1)
    )
)

(defun uri-path (uri-structure)
    (if 
        (not (is-empty-list uri-structure))
        (fifth uri-structure)
        (spawn-error 1)
    )
)

(defun uri-query (uri-structure)
    (if 
        (not (is-empty-list uri-structure))
        (sixth uri-structure)
        (spawn-error 1)
    )
)

(defun uri-fragment (uri-structure)
    (if 
        (not (is-empty-list uri-structure))
        (seventh uri-structure)
        (spawn-error 1)
    )
)

;==============================================================================
;                            FUNZIONE PRINCIPALE
;==============================================================================

(defun parse-uri (stringa)
    (cond
        ;----------------------------------------------------------------------
        ((not (stringp stringa))                         ; Non è una stringa
            (spawn-error 2)                              ; Error
        )                                                ; End COND-1
        ;----------------------------------------------------------------------
        ((equal (list-length (coerce stringa 'list)) 0)  ; Controllo N° Elem
            (spawn-error 3)                              ; Error
        )                                                ; End COND-2
        ;----------------------------------------------------------------------
        ((stringp stringa)                               ; È una stringa?
            (setq uri-structure '())                     ; Resetto Struttura
            (control-scheme                              ; Che schema è?
                (coerce stringa 'list)                   ; URI => list
                nil                                      ; NULL
            )                                            ; Call Function
        )                                                ; End COND-3
        ;----------------------------------------------------------------------
    )
)

;==============================================================================
;                          DEFINIZIONE DEBUG-TRACE
;==============================================================================

;(trace spawn-error)
;------------------------------------------------------------------------------
;(trace save-type)
;(trace save-data)
;------------------------------------------------------------------------------
;(trace is-empty-list)
;(trace is-one-element)
;------------------------------------------------------------------------------
;(trace is-host-ok)
;(trace is-ide-ok)
;(trace char-is-number)
;------------------------------------------------------------------------------
;(trace control-authority)
;(trace control-user)
;(trace control-user-tre)
;------------------------------------------------------------------------------
;(trace control-port)
;(trace control-path)
;(trace control-query)
;(trace control-fragment)
;------------------------------------------------------------------------------
;(trace control-host)
;(trace control-host-tre)
;------------------------------------------------------------------------------
;(trace control-scheme)
;(trace control-scheme-helper)
;------------------------------------------------------------------------------
;(trace parse-uri)

;==============================================================================
;                                       EOF
;==============================================================================